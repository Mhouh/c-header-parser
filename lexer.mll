{
open Parser
exception SyntaxError of string
}

let white = [' ' '\t']+
let newline = '\r' | '\n' | "\r\n"
let digit = ['0'-'9']
let lowercase = ['a'-'z']
let uppercase = ['A'-'Z']
let alpha = lowercase | uppercase

let int = '-'? digit digit* | "0x"(digit | ['A'-'F'])*
let id = ('_' | alpha)('_' | digit | alpha)*

rule read = parse
  | white { read lexbuf }
  | newline { Lexing.new_line lexbuf; read lexbuf }
  | int { INT_VAL (int_of_string (Lexing.lexeme lexbuf)) }
(* TYPES *)
  | "short" { SHORT }
  | "long" { LONG }
  | "signed" { SIGNED }
  | "unsigned" { UNSIGNED }
  | "void" { VOID }
  | "char" { CHAR }
  | "int" { INT }
  | "void" { VOID }
  | "float" { FLOAT }
  | "double" { DOUBLE }
(* KEYWORDS *)
  | "const" { CONST }
  | "typedef" { TYPEDEF }
  | "enum" { ENUM }
  | "struct" { STRUCT }
  | "union" { UNION }
(* SYMOBLS *)
  | '*' { TIMES }
  | ',' { COMMA }
  | ';' { SEMICOLON }
  | '=' { EQUAL }
  | '{' { L_BRACE } | '}' { R_BRACE }
  | '(' { L_PAR } | ')' { R_PAR }
  | '[' { L_SQUARE } | ']' { R_SQUARE }
  | id { IDENTIFIER (Lexing.lexeme lexbuf) }
  | _ { raise (SyntaxError ("Unexpected char: " ^ Lexing.lexeme lexbuf)) }
  | eof { EOF }
