%token TYPEDEF

%%

body:
  | typ = type_; alias = IDENTIFIER { Type typ, alias }
  | typ = named_type; alias = IDENTIFIER { Named_type typ, alias }
  | typ = anonymous_type; alias = IDENTIFIER { Anonymous_type typ, alias }
  | typ = prototype (L_PAR; TIMES; alias = IDENTIFIER; R_PAR { alias }) (* TODO funptr *)
    { Prototype (fst typ), snd typ }

%public
typedef:
  | TYPEDEF; t = body { Declaration.Typedef t }
