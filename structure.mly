%token STRUCT

%%

%public
struct_type:
  | STRUCT; id = IDENTIFIER { Type.Struct id }

%public
named_struct:
  | STRUCT; id = IDENTIFIER; body = structured_body { Named_type.Structure (body, id) }

%public
anonymous_struct:
  | STRUCT; body = structured_body { Anonymous_type.Structure body }
