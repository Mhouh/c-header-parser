%token TIMES
%token L_SQUARE, R_SQUARE

%%

%public
named_type:
  | n = named_enum { n }
  | n = named_struct { n }
  | n = named_union { n }

%public
anonymous_type:
  | a = anonymous_enum { a }
  | a = anonymous_struct { a }
  | a = anonymous_union { a }

user_defined:
  | t = enum_type { t }
  | t = struct_type { t }
  | t = union_type { t }

(* --- Array --- *)
constant:
  | i = INT_VAL { Constant.Litteral i }

const_type:
  | t = const_primitive { Primitive t }
  | t = user_defined { t }
  | id = IDENTIFIER { Alias id }

funptr (X): L_PAR; TIMES; x = X; R_PAR { x }

%public
typed (X):
  | CONST; t = const_type; x = X { t, x }
  | t = primitive; x = X { Primitive t, x }
  | t = user_defined; option (CONST); x = X { t, x }
  | id = IDENTIFIER; option (CONST); x = X { Alias id, x }
  | t = type_; TIMES; option (CONST); x = X { Type.Pointer t, x }
  | t = prototype (funptr(X)) { Type.Function_pointer (fst t), snd t }
  | t = typed (X); L_SQUARE; s = constant; R_SQUARE { Type.Array ((fst t), s), snd t }

%public
type_:
  | t = typed (nothing) { fst t }

%public
typed_identifier:
  | t = typed (IDENTIFIER) { t }
