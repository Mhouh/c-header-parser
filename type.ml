module Primitive = Primitive
module Enum = Enum
module Identifier = Identifier

type t =
  | Primitive of Primitive.t
  | Pointer of t
  | Array of t * Constant.t
  | Enum of string
  | Struct of string
  | Union of string
  | Function_pointer of t Function.Prototype.t
  | Alias of string
  
let rec to_string = function
  | Primitive p -> Primitive.to_string p
  | Pointer p -> "(ptr " ^ (to_string p) ^ ")"
  | Array (typ, size) -> "(array " ^ (Constant.to_string size) ^ " " ^ (to_string typ) ^ ")"
  | Enum s -> s
  | Struct s -> s
  | Union s -> s
  | Function_pointer p -> Function.Prototype.to_string to_string p
  | Alias a -> a
