module Named_type = Named_type
module Typedef = Typedef
module Function = Function

type t =
  | Named_type of Named_type.t
  | Typedef of Typedef.t
  | Function of Type.t Function.t

let bind = function
  | Named_type n -> Named_type.bind n
  | Typedef t -> Typedef.bind t
  | Function f -> Function.bind Type.to_string f

let write t = print_endline (bind t)
