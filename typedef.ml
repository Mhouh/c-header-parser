type body =
  | Type of Type.t
  | Named_type of Named_type.t
  | Anonymous_type of Anonymous_type.t
  | Prototype of Type.t Function.Prototype.t

type t = body * Identifier.t

let typedef typ alias = 
  "let " ^ alias ^ "=typedef" ^ typ ^ "\"" ^ alias ^ "\""

let bind (t, alias) =
  let module_name = Module.of_id alias in
  match t with
  | Type t ->
    let typ =
      let s = Type.to_string t in
      if String.starts_with ~prefix:"(" s
      then s
      else " " ^ s in
    typedef typ alias 
  | Named_type n -> Module.put (Named_type.bind n ^ " " ^ (typedef (" " ^ "t" (* TODO something like Named_type.name n *))  alias)) ~module_name
  | Anonymous_type a -> Module.put (Anonymous_type.typedef a alias) ~module_name
  | Prototype p -> Function.Prototype.typedef Type.to_string p alias
