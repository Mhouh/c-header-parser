type signedness = Signed | Unsigned
type t =
  | Void
  | Char of signedness option
  | Int of signedness * [ `Long | `Long_long | `Short ] option
  | Float
  | Double of [ `Long ] option
val to_string : t -> string
module C : sig
  val to_string : t -> string
end
