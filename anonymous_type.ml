type t =
  | Enum of Enum.Body.t
  | Structure of Structured.Body.t
  | Union of Structured.Body.t

let typedef t =
  match t with
  | Enum body -> Enum.Body.typedef body
  | Structure body -> Structured.Body.typedef "structure" body
  | Union body -> Structured.Body.typedef "union" body
