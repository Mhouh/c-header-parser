type t =
  | Litteral of int
  | Identifier of string

let to_string = function
  | Litteral i -> string_of_int i
  | Identifier s -> s
