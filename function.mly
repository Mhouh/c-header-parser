%%

argument:
  | t = type_ { t, None }
  | t = typed_identifier { fst t, Some (snd t) }

arguments:
  | L_PAR; args = separated_list (COMMA, argument); R_PAR { args }

%public
prototype (X):
  | return = type_; x = X; args = arguments { (return, args), x }

%public
function_declaration:
  | dec = prototype (IDENTIFIER) { Declaration.Function dec }

