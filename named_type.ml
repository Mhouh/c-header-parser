type t =
  | Enum of Enum.t
  | Structure of Structure.t
  | Union of Union.t

let bind = function
  | Enum e -> Enum.bind e
  | Structure s -> Structure.bind s
  | Union u -> Union.bind u
