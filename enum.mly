%token ENUM

%%

enum_constant:
  | id = IDENTIFIER { id }
  | id = IDENTIFIER; EQUAL; INT_VAL { id }
  | id = IDENTIFIER; EQUAL; IDENTIFIER { id }

enum_body:
  | L_BRACE; body = separated_list (COMMA, enum_constant); R_BRACE { body }

enum (X):
  | ENUM; x = X; body = enum_body { body, x }

%public enum_type:
  | ENUM; id = IDENTIFIER { Enum id }

%public named_enum:
  | e = enum (IDENTIFIER) { Named_type.Enum (fst e, snd e) }

%public anonymous_enum:
  | e = enum (nothing) { Anonymous_type.Enum (fst e) }
