%token <string> IDENTIFIER
%token <int> INT_VAL

%token COMMA, SEMICOLON
%token EQUAL
%token L_BRACE, R_BRACE
%token L_PAR, R_PAR

%token EOF

%start <Header.Declaration.t> declaration
%%

%public nothing: {}

declaration_body:
  | f = function_declaration { f }
  | t = typedef { t }

declaration:
  | body = declaration_body; SEMICOLON { body }
  | SEMICOLON; dec = declaration { dec }
  | EOF { raise End_of_file };
