%%

field:
  | id = typed_identifier; SEMICOLON { id }

%public
structured_body:
  | L_BRACE; fields = list (field); R_BRACE { fields }
