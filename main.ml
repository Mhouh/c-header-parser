open Lexer
open Lexing

open Printf

module Header = Header

let print_position outx lexbuf =
  let pos = lexbuf.lex_curr_p in
  fprintf outx "%s:%d:%d" pos.pos_fname
    pos.pos_lnum (pos.pos_cnum - pos.pos_bol + 1)

let parse_with_error lexbuf =
  try Parser.declaration Lexer.read lexbuf with
  | SyntaxError msg ->
    fprintf stderr "%a: %s\n" print_position lexbuf msg;
    exit (-1)
  | Parser.Error ->
    fprintf stderr "%a: parsing error\n" print_position lexbuf;
    exit (-1)

let rec parse acc lexbuf =
  try
    let declaration = parse_with_error lexbuf in
    parse (declaration :: acc) lexbuf
  with End_of_file -> acc

let loop filename inx =
  let lexbuf = Lexing.from_channel inx in
  lexbuf.lex_curr_p <- { lexbuf.lex_curr_p with pos_fname = filename };
  let declarations = List.rev (parse [] lexbuf) in
  close_in inx;
  declarations

let parse ~filename in_channel =
  loop filename in_channel
