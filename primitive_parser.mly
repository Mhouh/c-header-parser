%token CONST, SIGNED, UNSIGNED, SHORT, LONG
%token VOID, CHAR, INT, FLOAT, DOUBLE

%%

const_signed_short_primitive:
  { Int (Signed, Some `Short) }
  | INT { Primitive.Int (Signed, Some `Short) }
const_signed_long_long_primitive:
  { Int (Signed, Some `Long_long) }
  | INT { Primitive.Int (Signed, Some `Long_long) }
const_signed_long_primitive:
  { Int (Signed, Some `Long) }
  | LONG; s = const_signed_long_long_primitive { s }
  | INT { Primitive.Int (Signed, Some `Long) }
const_signed_primitive:
  { Int (Signed, None) }
  | SHORT; s = const_signed_short_primitive { s }
  | LONG; s = const_signed_long_primitive { s }
  | CHAR { Primitive.Int (Signed, None) }
  | INT { Primitive.Int (Signed, None) }
const_unsigned_short_primitive:
  { Int (Unsigned, Some `Short) }
  | INT { Primitive.Int (Unsigned, Some `Short) }
const_unsigned_long_long_primitive:
  { Int (Unsigned, Some `Long_long) }
  | INT { Primitive.Int (Unsigned, Some `Long_long) }
const_unsigned_long_primitive:
  { Int (Unsigned, Some `Long) }
  | LONG; s = const_unsigned_long_long_primitive { s }
  | INT { Primitive.Int (Unsigned, Some `Long) }
const_unsigned_primitive:
  { Int (Unsigned, None) }
  | SHORT; s = const_unsigned_short_primitive { s }
  | LONG; s = const_unsigned_long_primitive { s }
  | CHAR { Primitive.Int (Unsigned, None) }
  | INT { Primitive.Int (Unsigned, None) }
const_short_primitive:
  { Int (Signed, Some `Short) }
  | SIGNED; s = const_signed_short_primitive { s }
  | UNSIGNED; s = const_unsigned_short_primitive { s }
  | INT { Primitive.Int (Signed, Some `Short) }
const_long_long_primitive:
  { Int (Signed, Some `Long_long) }
  | SIGNED; s = const_signed_long_long_primitive { s }
  | UNSIGNED; s = const_unsigned_long_long_primitive { s }
  | INT { Primitive.Int (Signed, Some `Long_long) }
const_long_primitive:
  { Int (Signed, Some `Long) }
  | SIGNED; s = const_signed_long_primitive { s }
  | UNSIGNED; s = const_unsigned_long_primitive { s }
  | LONG; s = const_long_long_primitive { s }
  | INT { Primitive.Int (Signed, Some `Long) }
  | DOUBLE { Primitive.Int (Signed, Some `Long) }
const_char_primitive:
  { Char (None) }
  | SIGNED { Primitive.Char (None) }
  | UNSIGNED { Primitive.Char (None) }
const_signed_long_int_primitive:
  { Int (Signed, Some `Long) }
  | LONG { Primitive.Int (Signed, Some `Long) }
const_signed_int_primitive:
  { Int (Signed, None) }
  | SHORT { Primitive.Int (Signed, None) }
  | LONG; s = const_signed_long_int_primitive { s }
const_unsigned_long_int_primitive:
  { Int (Unsigned, Some `Long) }
  | LONG { Primitive.Int (Unsigned, Some `Long) }
const_unsigned_int_primitive:
  { Int (Unsigned, None) }
  | SHORT { Primitive.Int (Unsigned, None) }
  | LONG; s = const_unsigned_long_int_primitive { s }
const_short_int_primitive:
  { Int (Signed, Some `Short) }
  | SIGNED { Primitive.Int (Signed, Some `Short) }
  | UNSIGNED { Primitive.Int (Signed, Some `Short) }
const_long_long_int_primitive:
  { Int (Signed, Some `Long_long) }
  | SIGNED { Primitive.Int (Signed, Some `Long_long) }
  | UNSIGNED { Primitive.Int (Signed, Some `Long_long) }
const_long_int_primitive:
  { Int (Signed, Some `Long) }
  | SIGNED { Primitive.Int (Signed, Some `Long) }
  | UNSIGNED { Primitive.Int (Signed, Some `Long) }
  | LONG; s = const_long_long_int_primitive { s }
const_int_primitive:
  { Int (Signed, None) }
  | SIGNED; s = const_signed_int_primitive { s }
  | UNSIGNED; s = const_unsigned_int_primitive { s }
  | SHORT; s = const_short_int_primitive { s }
  | LONG; s = const_long_int_primitive { s }
const_double_primitive:
  { Double (None) }
  | LONG { Primitive.Double (None) }
%public
const_primitive:
  | SIGNED; s = const_signed_primitive { s }
  | UNSIGNED; s = const_unsigned_primitive { s }
  | SHORT; s = const_short_primitive { s }
  | LONG; s = const_long_primitive { s }
  | VOID { Primitive.Void }
  | CHAR; s = const_char_primitive { s }
  | INT; s = const_int_primitive { s }
  | FLOAT { Primitive.Void }
  | DOUBLE; s = const_double_primitive { s }
signed_short_int_primitive:
  { Int (Signed, Some `Short) }
  | CONST { Primitive.Int (Signed, Some `Short) }
signed_short_primitive:
  { Int (Signed, Some `Short) }
  | CONST { Primitive.Int (Signed, Some `Short) }
  | INT; s = signed_short_int_primitive { s }
signed_long_long_int_primitive:
  { Int (Signed, Some `Long_long) }
  | CONST { Primitive.Int (Signed, Some `Long_long) }
signed_long_long_primitive:
  { Int (Signed, Some `Long_long) }
  | CONST { Primitive.Int (Signed, Some `Long_long) }
  | INT; s = signed_long_long_int_primitive { s }
signed_long_int_primitive:
  { Int (Signed, Some `Long) }
  | CONST { Primitive.Int (Signed, Some `Long) }
signed_long_primitive:
  { Int (Signed, Some `Long) }
  | CONST { Primitive.Int (Signed, Some `Long) }
  | LONG; s = signed_long_long_primitive { s }
  | INT; s = signed_long_int_primitive { s }
signed_char_primitive:
  { Char (Some Signed) }
  | CONST { Primitive.Char (Some Signed) }
signed_int_primitive:
  { Int (Signed, None) }
  | CONST { Primitive.Int (Signed, None) }
signed_primitive:
  { Int (Signed, None) }
  | CONST; s = const_signed_primitive { s }
  | SHORT; s = signed_short_primitive { s }
  | LONG; s = signed_long_primitive { s }
  | CHAR; s = signed_char_primitive { s }
  | INT; s = signed_int_primitive { s }
unsigned_short_int_primitive:
  { Int (Unsigned, Some `Short) }
  | CONST { Primitive.Int (Unsigned, Some `Short) }
unsigned_short_primitive:
  { Int (Unsigned, Some `Short) }
  | CONST { Primitive.Int (Unsigned, Some `Short) }
  | INT; s = unsigned_short_int_primitive { s }
unsigned_long_long_int_primitive:
  { Int (Unsigned, Some `Long_long) }
  | CONST { Primitive.Int (Unsigned, Some `Long_long) }
unsigned_long_long_primitive:
  { Int (Unsigned, Some `Long_long) }
  | CONST { Primitive.Int (Unsigned, Some `Long_long) }
  | INT; s = unsigned_long_long_int_primitive { s }
unsigned_long_int_primitive:
  { Int (Unsigned, Some `Long) }
  | CONST { Primitive.Int (Unsigned, Some `Long) }
unsigned_long_primitive:
  { Int (Unsigned, Some `Long) }
  | CONST { Primitive.Int (Unsigned, Some `Long) }
  | LONG; s = unsigned_long_long_primitive { s }
  | INT; s = unsigned_long_int_primitive { s }
unsigned_char_primitive:
  { Char (Some Unsigned) }
  | CONST { Primitive.Char (Some Unsigned) }
unsigned_int_primitive:
  { Int (Unsigned, None) }
  | CONST { Primitive.Int (Unsigned, None) }
unsigned_primitive:
  { Int (Unsigned, None) }
  | CONST; s = const_unsigned_primitive { s }
  | SHORT; s = unsigned_short_primitive { s }
  | LONG; s = unsigned_long_primitive { s }
  | CHAR; s = unsigned_char_primitive { s }
  | INT; s = unsigned_int_primitive { s }
short_int_primitive:
  { Int (Signed, Some `Short) }
  | CONST { Primitive.Int (Signed, Some `Short) }
short_primitive:
  { Int (Signed, Some `Short) }
  | CONST { Primitive.Int (Signed, Some `Short) }
  | SIGNED; s = signed_short_primitive { s }
  | UNSIGNED; s = unsigned_short_primitive { s }
  | INT; s = short_int_primitive { s }
long_long_int_primitive:
  { Int (Signed, Some `Long_long) }
  | CONST { Primitive.Int (Signed, Some `Long_long) }
long_long_primitive:
  { Int (Signed, Some `Long_long) }
  | CONST { Primitive.Int (Signed, Some `Long_long) }
  | SIGNED; s = signed_long_long_primitive { s }
  | UNSIGNED; s = unsigned_long_long_primitive { s }
  | INT; s = long_long_int_primitive { s }
long_int_primitive:
  { Int (Signed, Some `Long) }
  | CONST { Primitive.Int (Signed, Some `Long) }
long_double_primitive:
  { Double (Some `Long) }
  | CONST { Primitive.Double (Some `Long) }
long_primitive:
  { Int (Signed, Some `Long) }
  | CONST; s = const_long_primitive { s }
  | SIGNED; s = signed_long_primitive { s }
  | UNSIGNED; s = unsigned_long_primitive { s }
  | LONG; s = long_long_primitive { s }
  | INT; s = long_int_primitive { s }
  | DOUBLE; s = long_double_primitive { s }
void_primitive:
  { Void }
  | CONST { Primitive.Void }
char_primitive:
  { Char (None) }
  | CONST; s = const_char_primitive { s }
  | SIGNED; s = signed_char_primitive { s }
  | UNSIGNED; s = unsigned_char_primitive { s }
int_primitive:
  { Int (Signed, None) }
  | CONST { Primitive.Int (Signed, None) }
  | SIGNED; s = signed_int_primitive { s }
  | UNSIGNED; s = unsigned_int_primitive { s }
  | SHORT; s = short_int_primitive { s }
  | LONG; s = long_int_primitive { s }
float_primitive:
  { Float }
  | CONST { Primitive.Float }
double_primitive:
  { Double (None) }
  | CONST; s = const_double_primitive { s }
  | LONG; s = long_double_primitive { s }
%public
primitive:
  | SIGNED; s = signed_primitive { s }
  | UNSIGNED; s = unsigned_primitive { s }
  | SHORT; s = short_primitive { s }
  | LONG; s = long_primitive { s }
  | VOID; s = void_primitive { s }
  | CHAR; s = char_primitive { s }
  | INT; s = int_primitive { s }
  | FLOAT; s = float_primitive { s }
  | DOUBLE; s = double_primitive { s }
