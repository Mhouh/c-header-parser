let _bind typ body_bindings identifier =
  let start = "let s=" ^ typ ^ "\"" ^ identifier ^ "\"" in
  List.fold_left (fun acc b -> acc ^ b ^ " ") start body_bindings
  ^ "let()=seal s"

module Body = struct
  type t = (Type.t * Identifier.t) list

  let escape c = c (* TODO *)

  let binding ocaml c typ = "let " ^ ocaml ^ "=field s\"" ^ c ^ "\"" ^ typ

  let bindings t = List.map (fun (typ, id) -> binding (escape id) id (Type.to_string typ)) t

  let typedef typ t alias = _bind typ (bindings t) alias ^ (" let " ^ alias ^ "=typedef " ^ alias ^ "\"" ^ alias ^ "\"")
end

type t = Body.t * Identifier.t

let to_module id = (String.capitalize_ascii id) (* TODO *)

let bind typ (body, identifier) =
  _bind typ (Body.bindings body) identifier
