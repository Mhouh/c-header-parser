%token UNION

%%

%public
union_type:
  | UNION; id = IDENTIFIER { Type.Union id }

%public
named_union:
  | UNION; id = IDENTIFIER; body = structured_body { Named_type.Union (body, id) }

%public
anonymous_union:
  | UNION; body = structured_body { Anonymous_type.Union body }
