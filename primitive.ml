type signedness = Signed | Unsigned

type t =
  | Void
  | Char of signedness option
  | Int of signedness * [ `Short | `Long | `Long_long ] option
  | Float
  | Double of [ `Long ] option

let to_string =
  let string_of_sign = function
    | Signed -> "s"
    | Unsigned -> "u" in
  function
  | Void -> "void"
  | Char so ->
    begin
      match so with
      | None -> ""
      | Some sign -> string_of_sign sign
    end ^ "char"
  | Int (s, l) ->
    let sign =
      match s with
      | Signed -> (fun s -> match s with "" -> "int" | s -> s)
      | Unsigned -> (fun s -> match s with "" -> "uint" | s -> "u" ^ s) in
    let length =
      match l with
      | None -> ""
      | Some `Short -> "short"
      | Some `Long -> "long"
      | Some `Long_long -> "llong" in
    sign length
  | Float -> "float"
  | Double lo ->
    let length = match lo with None -> "" | Some `Long -> "l" in
    length ^ "double"

module C = struct
  let string_of_sign = function
    | Signed -> "signed"
    | Unsigned -> "unsigned"

  let to_string = function
    | Void -> "void"
    | Char so ->
      begin
        match so with
        | None -> ""
        | Some sign -> string_of_sign sign ^ " "
      end ^ "char"
    | Int (s, l) ->
      let sign =
        match s with
        | Signed -> (fun s -> match s with "" -> "int" | s -> s)
        | Unsigned -> ( ^ ) "unsigned " in
      let length =
        match l with
        | None -> ""
        | Some `Short -> "short"
        | Some `Long -> "long"
        | Some `Long_long -> "long long" in
      sign length
    | Float -> "float"
    | Double lo ->
      let length = match lo with None -> "" | Some `Long -> "long " in
      length ^ "double"
end
