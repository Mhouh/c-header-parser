let _bind ?(typedef=false) typ constants body_bindings identifier =
  let start = typ ^ " " ^ constants ^ "let t=enum" ^ (if typedef then "~typedef:true" else "") ^ "\"" ^ identifier ^ "\"[" in
  List.fold_left ( ^ ) start body_bindings
  ^ "]"

module Body = struct
  type t = Identifier.t list

  let escape c = c (* TODO *)

  let types t = match t with [] -> "" | h :: t -> List.fold_left (fun acc t -> acc ^ "|" ^ t) ("type t=" ^ h) t
  let constant c = "let " ^ (escape c) ^ "=constant\"" ^ c ^ "\"int64_t"
  let constants t = List.fold_left (fun acc c -> acc ^ (constant c) ^ " ") "" t

  let binding ocaml c = ocaml ^ "," ^ c ^ ";"

  let bindings t =
    let ids = List.map Identifier.to_string t in
    List.map2 binding (List.map escape ids) ids

  let typedef t alias = _bind ~typedef:true (types t) (constants t) (bindings t) alias
end

type t = Body.t * Identifier.t

let bind (body, identifier) =
  _bind (Body.types body) (Body.constants body) (Body.bindings body) identifier
