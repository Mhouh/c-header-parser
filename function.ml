let escape = Fun.id (* TODO *)

module Prototype = struct
  module Argument = struct
    type 't t = 't * Identifier.t option
  end

  type 't t = 't * 't Argument.t list

  let to_string string_of_type (return_type, arguments) =
    let add_argument = (fun acc t -> Fun.flip ( ^ ) (string_of_type (fst t) ^ "@->") acc) in
    List.fold_left add_argument "(" arguments ^ (string_of_type return_type) ^ ")"

  let typedef string_of_type (return, args) identifier = "let " ^ (escape identifier) ^ "=foreign\"" ^ identifier ^ "\"" ^ (to_string string_of_type (return, args))
end

type 't t = 't Prototype.t * Identifier.t

let bind string_of_type ((return, args), identifier) = "let " ^ (escape identifier) ^ "=foreign\"" ^ identifier ^ "\"" ^ (Prototype.to_string string_of_type (return, args))
